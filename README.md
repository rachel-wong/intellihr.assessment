## Scrabble Challenge

[Instructions](instructions.md)

The project is accessible here https://intellihr-scrabble.netlify.com

Alternatively, you can download all files and open `index.html` using a browser of your choice on your local machine.

### Understanding the Instructions

- You can have duplicate letters in a hand (based on real scrabble and also the words in the dictionary)
- Assuming every drawing of the hand, you get a fresh new set of characters
- Assuming every submission of a word it is a new word, and not building on a previous word
- Now you can get more letters from the bag after successfully submitting a word, just like in the real game.
- String concatenation in loops when generating a hand is discouraged based on research
- A game consists of submitting words, each of which can calculate a score. A game has multiple hands, and a game can have one overall total score.
- I researched into patricia trie but that was really difficult to understand
- This is an exercise in logic, so I boostrapped the UI

### :bomb: What is not working

- Attempted to write my own function to generate permutations from a hand (array) that is both existing in dictionary and is the highest scoring word out of all words, but did not work. Couldn't get the backtracking done correctly.
- Currently this version is all client-side, which - while it helps me to understand the problem - is not ideal for what the application needs to do. Every time the submit button is clicked, an api call is made to the json and dictionary.txt, the data is then lost after the score is computed.
- This project needs something like NodeJS for better storing of scores, game history, log in, authentication, session tracking, temporarily storing of variables. Having a backend could allow a temporary storage of data (dictionary.txt, json) so it's better use of resources, especially once the application has been deployed.
- This isn't something that is malfunctioning per se. However, if user starts a new game > presses "Clear History" > "End Game" > "New Game", the new game will always ultimately start with a single game history entry of 0. This is because even if you play no words for a new game, there will always be a base score of 0 (and not null). A score of 0 will be recorded as a score still, and added to the game history array. **This is more of a question of, does the game consider a score of 0 to be worth storing?** 