function updateHand(word, hand) {
  word = word.split("")
  for (let i = 0; i < word.length; i++) {
    let removeIndex = hand.findIndex(char => char === word[i])
    hand.splice(removeIndex, 1)
  }
}

updateHand("keg", ["a", "b", "k", "k", "g", "e", "z"])
// remaining hand ["a", "b", "k", "z"]