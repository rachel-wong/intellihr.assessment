// DOM elements
const gameInterface = document.getElementById("game_interface")
const scoreboardInterface = document.getElementById("scoreboard_interface")

const submitBtn = document.getElementById("submitBtn")
const newHandBtn = document.getElementById("newHandBtn")
const newGameBtn = document.getElementById("newGameBtn")
const endGameBtn = document.getElementById("endGameBtn")
const newLettersBtn = document.getElementById("newLettersBtn")
const newPlayerBtn = document.getElementById('newPlayerBtn')
const clearHistoryBtn = document.getElementById("clearHistoryBtn")
const optimalBtn = document.getElementById("optimalBtn")

const wordInput = document.getElementById("wordInput")

const wordDisplay = document.getElementById("wordSubmitted")
const handDisplay = document.getElementById("handDisplay")
const scoreListDisplay = document.getElementById("scoreList")
const totalScoreDisplay = document.getElementById("totalScore")
const gameScoreListDisplay = document.getElementById("gameScoreList")

const alertText = document.getElementById('alertText')
const alertBox = document.getElementById('alertBox')
const messageText = document.getElementById('messageText')
const messageBox = document.getElementById('messageBox')

// // VIEW: Toggle the game interface
const displayUI = (toggle) => {
  if (toggle) {
    wordDisplay.innerHTML = ""
    handDisplay.innerHTML = ""
    scoreListDisplay.innerHTML = ""
    gameScoreListDisplay.innerHTML = ""
    gameInterface.style.display = "block"
    scoreboardInterface.style.display = "block"
    endGameBtn.style.display = "block"
    newHandBtn.style.display = "block"
    newGameBtn.style.display = "none"
    clearHistoryBtn.style.display = "block"
    optimalBtn.style.display = "block"
    newLettersBtn.style.display = "none"
  } else {
    wordDisplay.innerHTML = ""
    handDisplay.innerHTML = ""
    scoreListDisplay.innerHTML = ""
    gameScoreListDisplay.innerHTML = ""
    gameInterface.style.display = "none"
    scoreboardInterface.style.display = "none"
    endGameBtn.style.display = "none"
    newHandBtn.style.display = "none"
    newGameBtn.style.display = "block"
    clearHistoryBtn.style.display = "none"
    optimalBtn.style.display = "none"
    newLettersBtn.style.display = "none"
  }
}

// // VIEW: Dismissable notifications for UI and error
const displayAlert = (issue, error) => {
  if (error) {
    alertBox.classList.add("alert-warning")
  } else {
    alertBox.classList.add("alert-info")
  }
  alertContainer.style.display = "block"
  alertText.innerHTML = issue
}

// VIEW:  Display the score for the word submitted
const displayWordScore = (singleScore, word) => {
  let li = document.createElement("li")
  li.innerHTML = singleScore + " for " + word
  scoreListDisplay.appendChild(li)
}

// // VIEW: Clear fields
const clearSubmittedWord = () => {
  wordInput.value = ""
  // wordDisplay.innerHTML = ""
}

// VIEW: Display a single historical game score
const displayHistoryEntry = (historyScore) => {
  let li = document.createElement('li')
  li.innerHTML = historyScore
  gameScoreListDisplay.appendChild(li)
}

// CONTROLLER: Generate a new game instance
const initNewGame = () => {
  let currentScore = 0
  sessionStorage.clear()
  displayUI(true)
  displayAlert("New game has started", false)
  displayGameHistory()
  setCurrentGameScore(currentScore)
  displayCurrentGameScore()
}

// CONTROLLER: Generate a new hand
const generateNewHand = () => {
  newLettersBtn.style.display = "none"
  let newHand = getHand()
  sessionStorage.setItem('hand', JSON.stringify(newHand))
  let wordsFromHand = []
  localStorage.setItem('wordsFromHand', JSON.stringify(wordsFromHand))
  handDisplay.innerHTML = newHand
}

// CONTROLLER: Delete game instance
const terminateGame = () => {
  let currentGameScore = JSON.parse(sessionStorage.getItem("currentScore"))
  if (currentGameScore != null) {
    // allows zero scores
    setGameHistory(currentGameScore)
    sessionStorage.clear() // clear hand & currentScore
  } else {
    // no change to game score history 
    sessionStorage.clear() // clear hand & currentScore
  }
  displayAlert("Game ended. Goodbye.", false)

  displayUI(false)
}

// CONTROLLER: Clear History and Re-display
const clearHistory = () => {
  localStorage.clear()
  displayAlert("All previous game scores erased.", false)
  displayGameHistory()
}

// CONTROLLER: Get highest scoring word
// const generateBestWord = () => {
//   let currentHand = JSON.parse(sessionStorage.getItem('hand'))
//   if (currentHand) {
//     let possibilities = getAllPermutations(currentHand) // return array of permutations 
//     // let scores = possibilities.map(item => item = getWordScore(item))
//     // return possibilities[Math.max.apply(Math, scores)[0]]
//   } else {
//     displayAlert("You must generate a hand first.", true)
//     clearSubmittedWord()
//     return false
//   }
// }

// CONTROLLER: Generate a score for a word inputted
function generateWordScore() {
  let currentHand = JSON.parse(sessionStorage.getItem('hand'))
  let wordsFromHand = JSON.parse(localStorage.getItem('wordsFromHand'))
  let submittedWord = wordInput.value.trim().toLowerCase().replace(/ /g, '')
  let onlyAlphabets = /^[A-Za-z]+$/

  // Validation: No hand
  if (!currentHand) {
    displayAlert("You must generate a hand first.", true)
    clearSubmittedWord()
    return false
  }
  // Validation: No word OR word uses more letters than what is in hand (7)
  else if (!submittedWord || submittedWord.length > 7) {
    displayAlert("You must only use the number of letters in your hand.", true)
    clearSubmittedWord()
    return false
  }
  // Validation: Input has non-alphabetical chars
  else if (onlyAlphabets.test(submittedWord) == false) {
    displayAlert("Alphabets only, no numbers or symbols.", true)
    clearSubmittedWord()
    return false
  }
  // Validation: Is input using only what is in the hand?
  else if (checkEnoughLetters(currentHand, submittedWord) == false) {
    displayAlert("You must use only the alphabets in your hand.", true)
    clearSubmittedWord()
    return false
  }
  // Validation: Same word submitted using the same hand of letters
  else if (wordsFromHand.includes(submittedWord) == true) {
    displayAlert("You have submitted this word before using this current hand of letters. Try a different word or generate a new hand.", true)
    clearSubmittedWord()
    return false
  } else {
    wordDisplay.innerHTML = submittedWord
    wordsFromHand.push(submittedWord)
    localStorage.setItem("wordsFromHand", JSON.stringify(wordsFromHand))
    getScore(submittedWord, currentHand)
    clearSubmittedWord()
  }
}

// MODEL: Update current game score to storage
function setCurrentGameScore(currentScore) {
  let storedScore = JSON.parse(sessionStorage.getItem("currentScore"))
  if (!storedScore) {
    sessionStorage.setItem("currentScore", currentScore)
  } else {
    let newScore = parseInt(storedScore + currentScore)
    sessionStorage.setItem("currentScore", JSON.stringify(newScore))
  }
}

// MODEL: Update game history score array
function setGameHistory(gameTotalScore) {
  let allSavedHistory = JSON.parse(localStorage.getItem("historyScores"))
  allSavedHistory.push(gameTotalScore)
  localStorage.setItem('historyScores', JSON.stringify(allSavedHistory))
}

// UTIL: Random letters generator
// Learning from https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
// and from https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
function getHand() {
  let hand = []
  let alphabets = "abcdefghijklmnopqrstuvwxyz"
  let n = 7 // This needs to be changed if the exercise changes the length of the word. 
  for (let i = 0; i < n; i++) {
    // let index = Math.floor(Math.random() * 27)
    // hand.push(alphabet[index])
    hand.push(alphabets.charAt(Math.floor(Math.random() * 26)))
  }
  return hand
}

// UTIL: Check word submitted is only using letters in hand.
// All of submitted word must be in the hand (hand > submittedWord)
function checkEnoughLetters(hand, word) {
  let count = {} // count up all of hand
  hand.forEach(char => {
    count[char] = (count[char] || 0) + 1
  })
  word = word.split("")
  for (let i = 0; i < word.length; i++) {
    if (count[word[i]]) {
      count[word[i]] -= 1
    }
    // if submitted letter doesn't exist in hand count => false
    else if (count[word[i]] === undefined) {
      return false
    } else {
      count[word[i]] -= 1
    }
  }
  // If submitted letter takes more out of hand (-ive count) => false
  let enoughLetters = Object.values(count).filter(val => {
    return val < 0
  })
  return enoughLetters.length < 0 ? false : true
}

// UTIL: Binary search for word in dictionary array (my first ever!)
// Also researched Patricia Trie
// Learning from https://stackoverflow.com/questions/22697936/binary-search-in-javascript
// and from https://dev.to/stephjs/linear-and-binary-search-in-javascript-4b7h 
// and from https://medium.com/@jeffrey.allen.lewis/javascript-algorithms-explained-binary-search-25064b896470
function searchWord(dictionary, target) {
  let startIndex = 0
  let endIndex = dictionary.length - 1
  // Iterate while start not meets end 
  while (startIndex <= endIndex) {
    let midIndex = Math.floor((startIndex + endIndex) / 2)
    // If element is right in the current middle of the array, then found! 
    if (dictionary[midIndex] === target) {
      return true
    }
    // Move to the right side of the current middle of the array 
    else if (dictionary[midIndex] < target) {
      startIndex = midIndex + 1
    }
    // Move to the left side of the current middle of the array
    else {
      endIndex = midIndex - 1
    }
  }
  // Word is definitely not in the array when start of array meets the end of array
  return false
}

// UTIL: Calculate score based on JSON & Dictionary 
// MODEL: Update storage with score for word
async function getScore(submittedWord, currentHand) {
  try {
    let [charValues, dictionary] = await Promise.all([
      fetch('/assets/letter-values.json').then(response => response.json()),
      fetch('/assets/words.txt').then(data => data.text())
    ])
    let target = submittedWord.split("").map(char => char.toUpperCase()).join("")
    dictionary = dictionary.split("\n").filter(word => word.length <= 7) // need to change this .filter if exercise changes length of hand
    let found = searchWord(dictionary, target)

    // This dictionary check should ideally be another validation check 
    if (found == true) {
      // Calculate score from charValues
      let score = submittedWord.split("").reduce((a, b) => {
        return a += charValues[b]
      }, 0)
      if (submittedWord.length === currentHand.length) {
        score = score * submittedWord.length + 50
      } else {
        score = score * submittedWord.length
      }
      displayWordScore(score, submittedWord)
      updateHand(submittedWord, currentHand)
      setCurrentGameScore(score)
      displayCurrentGameScore()
    } else {
      displayAlert("Word is not found in dictionary. Try again", true)
      clearSubmittedWord()
      return false
    }
  } catch (err) {
    displayAlert(err)
  }
}

// CONTROLLER: Get more letters after a word successfully submitted 
function getLetters() {
  let currentHand = JSON.parse(sessionStorage.getItem("hand"))
  let newHand = getHand()
  newHand.length -= currentHand.length
  currentHand = currentHand.concat(newHand)
  sessionStorage.setItem("hand", JSON.stringify(currentHand))
  let wordsFromHand = []
  sessionStorage.setItem('wordsFromHand', JSON.stringify(wordsFromHand))
  handDisplay.innerHTML = ""
  handDisplay.innerHTML = currentHand
  newLettersBtn.style.display = "none"
}

// VIEW: Update hand and to display the letters remaining after successful submission of word
// Turn on the get more letters button if there is at least 1 letter remaining in hand
function updateHand(word, hand) {
  word = word.split("")
  // allow for duplicate letters in the hand
  word.forEach(char => {
    let removeIndex = hand.findIndex(handItem => {
      return char === handItem
    })
    if (removeIndex != -1) hand.splice(removeIndex, 1)
  })
  if (hand.length != 0) {
    newLettersBtn.style.display = "block"
  } else {
    displayAlert("You have ran out of words!", true)
    newLettersBtn.style.display = "none"
  }
  sessionStorage.setItem('hand', JSON.stringify(hand))
  handDisplay.innerHTML = hand
}

// VIEW: Display total score for current game total score
function displayCurrentGameScore() {
  let currentScore = JSON.parse(sessionStorage.getItem("currentScore"))
  totalScoreDisplay.innerHTML = currentScore.toString()
}

// VIEW:  Display previous game total scores
function displayGameHistory() {
  gameScoreListDisplay.innerHTML = ""
  let history = JSON.parse(localStorage.getItem('historyScores'))
  if (history != null && history.length != 0) {
    history.forEach(score => {
      if (score != null) {
        displayHistoryEntry(score.toString())
      }
    })
  } else if (history === []) {
    displayHistoryEntry("No history available")
  } else {
    // if no history, set up a new game history for session
    let newHistory = []
    localStorage.setItem("historyScores", JSON.stringify(newHistory))
    displayHistoryEntry("No history available")
  }
}

// EVENT LISTENER: Generate New Game
newGameBtn.addEventListener("click", initNewGame, true)

// EVENT LISTENER: Generate New Hand
newHandBtn.addEventListener("click", generateNewHand, true)

// EVENT LISTENER: Generate a score for a word submitted into the form
submitBtn.addEventListener("click", generateWordScore, true)

// EVENT LISTENER: End game session
endGameBtn.addEventListener("click", terminateGame, true)

// EVENT LISTENER: Clear Game History
clearHistoryBtn.addEventListener("click", clearHistory, true)

// EVENT LISTENER: Get More Letters for Hand
newLettersBtn.addEventListener("click", getLetters, true)

// EVENT LISTENER: Get optimal scoring word
// optimalBtn.addEventListener("click", generateBestWord, true)